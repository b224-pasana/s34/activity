// ACTIVITY

/*
1. Create a GET route that will access the /home route that will print out
a simple message.
2. Process a GET request at the /home route using postman.
3. Create a GET route that will access the /users route that will retrieve
all the users in the mock database.
4. Process a GET request at the /users route using postman.
5. Create a DELETE route that will access the /delete-user route to
remove a user from the mock database.
6. Process a DELETE request at the /delete-user route using postman.
*/
const express = require("express");
const app = express();
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// simple registration
let users = [];

app.post("/signup", (request, response) => {
	if(request.body.username !== '' && request.body.password !== ''){
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`);
	} else {
		response.send("Please input BOTH username and password.")
	}
});

// 1
app.get("/home", (request, response) => {
	response.send("Hello from the /home endpoint!")
});

// 2
app.get("/users", (request, response) => {
	response.send(users);
    console.log(users);
});

// 3
app.delete("/delete-user", (request, response) => {
   
    for(let i = 0; i < users.length; i++){

        // function userIndex(){
        //     if(request.body.username == users[i].username){
        //         console.log(users[i].username)
        //     }
        // } userIndex(request.body.username)
        if(request.body.username === users[i].username && request.body.password === users[i].password) {
           
            delete(users[i]);
            console.log(users);
            response.send(`Deleted user: ${request.body.username}`);
          } else if(request.body.username == users[i].username && request.body.password !== users[i].password) {
            response.send(`Username: ${request.body.username}
            Please enter your correct password.`);
          } else if(request.body.username !== users[i].username || request.body.password !== users[i].password) {
            response.send(`User does not exist`);
          }
      
    }
    
});


app.listen(port, ()=> console.log(`Server running at port ${port}`));